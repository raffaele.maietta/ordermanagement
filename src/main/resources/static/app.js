var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#orderList").show();
        $("#mainForm").show();
    }else {
        $("#orderList").hide();
        $("#mainForm").hide();
    }
    $("#orders").html("");
}

function connect() {
    var socket = new SockJS('/order-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe("/order/"+$('#customer option:selected').val()+"/updates", function (order) {
            showUpdate(JSON.parse(order.body));
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendOrder() {
	const order = new Object();
	order.customer  = $('#customer option:selected').val();
	order.total  = Number($('#total').val());
	order.product  = $('#productSelect option:selected').val();
	console.log(JSON.stringify(order));
	const topic = "/async/order/"+order.customer+"/add";
    stompClient.send(topic, {}, JSON.stringify(order));
}

function showUpdate(message) {
    $("#orders").append("<tr><td>" + message.product + "</td><td>" + message.reference + "</td><td>" + message.date + "</td><td>" + message.total + "</td><td>" + message.approved + "</td><td>" + message.approvalDate + "</td></tr>");
}

$(document).ready(function() {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#orderList").hide();
    $("#mainForm").hide();
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendOrder(); });
    $("#productSelect").on('change', function() {
    	var selected = $('#productSelect option:selected').val();
    	var total = Number(0);
    	if(selected === "Mi TV 4S 43\""){
    		total = Number(449.89);
    	}else if(selected === "APPLE iPhone 11 Pro Max 256GB"){
    		total =  Number(1399.00);
    	}else{
    		total =  Number(559.00);
    	}
    	$( "#total" ).val(total);
    });
});
package com.accenture.ordermanagement.repository;

import org.springframework.data.repository.CrudRepository;
import com.accenture.ordermanagement.model.Order;


public interface OrderRepository extends CrudRepository<Order, Long>{
}
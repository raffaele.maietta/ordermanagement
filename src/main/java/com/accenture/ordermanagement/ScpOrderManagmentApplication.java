package com.accenture.ordermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScpOrderManagmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScpOrderManagmentApplication.class, args);
	}
}

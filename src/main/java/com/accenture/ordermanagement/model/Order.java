package com.accenture.ordermanagement.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "\"Order\"")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Order implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name ="id", unique = true)
	private long id;
	@Column(name ="reference", unique=true)
	private String reference;
	@Column(name ="product")
	private String product;
	@Column(name ="total")
	private double total;
	@Column(name ="date")
	private Date date;
	@Column(name ="customer")
	private String customer;
	@Column(name ="approved")
	private Boolean approved;
	@Column(name ="approval_date")
	private Date approvalDate;
}

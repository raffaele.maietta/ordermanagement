package com.accenture.ordermanagement.controller;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.ordermanagement.model.Order;
import com.accenture.ordermanagement.service.OrderService;

@RestController
public class OrderController {

	Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private OrderService orderService;

	@RequestMapping("/order/count")
	public long count() {
		log.info("Search total number of Orders");
		return orderService.getCount();
	}

	@RequestMapping(value ="/order/all", produces = "application/json")
	public List<Order> getAllOrders(){
		log.info("Searching all Orders");
		return orderService.findAllOrder();
	}

	@RequestMapping(method=RequestMethod.POST, value = "/order/add", consumes = "application/json")
	public Order addOrder(@RequestBody Order order) throws Exception{
		order.setReference(UUID.randomUUID().toString());
		order.setApproved(false);
		order.setDate(new Date());
		log.info("Creation/Updating Order - "+order);
		Thread.sleep(3000); // simulated delay
		order.setApproved(true);
		order.setApprovalDate(new Date());
		order = orderService.insertOrder(order);
		log.info("Order - "+ order);
		return order;
	}

	@MessageMapping("/order/{customer}/add")
	@SendTo("/order/{customer}/updates")
	public Order asyncAddOrder(@RequestBody Order order, @DestinationVariable String customer) throws Exception {
		log.info("customer: " + customer);
		order.setReference(UUID.randomUUID().toString());
		order.setApproved(false);
		order.setDate(new Date());
		log.info("Creation/Updating Order - "+order);
		Thread.sleep(3000); // simulated delay
		order.setApproved(true);
		order.setApprovalDate(new Date());
		order = orderService.insertOrder(order);
		log.info("Order - "+ order);
		return order;
	}

	@RequestMapping("/order/id/{id}" )
	public Order findById(@PathVariable long id) {
		log.info("Searching Order with ID - "+ id);
		return orderService.findOrderById(id);
	}

	@RequestMapping(method=RequestMethod.DELETE, value="/order/delete/{id}")
	public boolean deleteOrder(@PathVariable long id) {
		return orderService.deleteOrder(id);
	}

}

package com.accenture.ordermanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.ordermanagement.model.Order;
import com.accenture.ordermanagement.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository OrderRepository;

	public long getCount() {
		long count = OrderRepository.count();
		return count;
	}

	public List<Order> findAllOrder(){
		List<Order> Order = new ArrayList<>();
		OrderRepository.findAll().forEach(Order::add);
		Order.forEach(System.out::println);
		return Order;
	}

//	public boolean insertOrder(Order Order) {
//		try {
//			OrderRepository.save(Order);
//			return true;
//		} 
//		catch (Exception e) {
//			return false;
//		}
//	}
	
	public Order insertOrder(Order Order) {
		try {
			return OrderRepository.save(Order);
		} 
		catch (Exception e) {
			return new Order();
		}
	}

	public Order findOrderById(Long id) {
		Order Order = OrderRepository.findById(id).orElse(null);
		return Order;
	}

	public boolean deleteOrder(long id) {
		Order Order = OrderRepository.findById(id).orElse(null);
		if(Order!=null) {
			OrderRepository.delete(Order);
			return true;
		}
		return false;
	}
}
